Pod::Spec.new do |s|
  s.name             = 'Docus'
  s.version          = '1.4.3'
  s.summary          = 'Docus is used to scan the documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/docus_ios'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }
  s.source           = {:http => 'https://www.repo2.frslabs.space/repository/docus-ios/docus-ios/1.4.3/Docus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '12.0'
  s.ios.vendored_frameworks = 'Docus.framework'
  s.swift_version = '5.0'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'}
end
