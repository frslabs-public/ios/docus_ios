Pod::Spec.new do |s|
  s.name             = 'Docus'
  s.version          = '1.2.0'
  s.summary          = 'Docus is used to scan the documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/docus_ios'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }
  s.source           = {:http => 'https://docus-ios.repo.frslabs.space/docus-ios/1.2.0/Docus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Docus.framework'
  s.swift_version = '5.0'
end
