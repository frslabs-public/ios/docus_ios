#
# Be sure to run `pod lib lint Docus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Docus'
  s.version          = '9.0.2'
  s.summary          = 'Docus is used to scan the documents'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/docus_ios'
  s.license          = 'MIT'
  s.author           = {'sravani' => 'shravani@frslabs.com' }
  s.source           = {:http => 'https://docus-ios.repo.frslabs.space/docus-ios/9.0.2/Docus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Docus.framework'
  s.swift_version = '5.0'
end
  
